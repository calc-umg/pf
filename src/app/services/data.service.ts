import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  private tema:Tema[] = [
    {
        idx: 0,
        titulo: "Ecuaciones y Desigualdades",
        contenido: [
          {
            titulo: 'Ecuación',
            contenido: 'Una ecuación es una igualdad matemática entre dos expresiones, denominadas miembros y separadas por el signo igual, en las que aparecen elementos conocidos y datos desconocidos o incógnitas, relacionados mediante operaciones matemáticas. Los valores conocidos pueden ser números, coeficientes o constantes; también variables o incluso objetos complejos como funciones o vectores, los elementos desconocidos pueden ser establecidos mediante otras ecuaciones de un sistema, o algún otro procedimiento de resolución de ecuaciones. En álgebra, una ecuación es una expresión matemática que contiene un signo de igualdad. Una ecuación nos indica que dos expresiones representan el mismo número. Por ejemplo, y=12x es una ecuación. ',
            teorema: '',
            ejemplo: '1'
          },
          {
            titulo: 'Desigualdades',
            contenido: 'Una inecuación es una desigualdad algebraica en la cual los conjuntos (miembros) se encuentran relacionados por los signos < (menor que), ≤ (menor o igual que), > (mayor que) y ≥ (mayor o igual que). Una desigualdad es una expresión matemática que contiene signos de desigualdad. Por ejemplo, y≤12x es una desigualdad. Las desigualdades son utilizadas para indicar que una expresión es mayor, o bien, que es menor que otra expresión. Tanto ecuaciones como desigualdades pueden contener variables y constantes. ',
            teorema: 'teo1',
            ejemplo: '2'
          },
        ],
        subtemas: [
        /*  {
            titulo: 'titulo 1',
            contenido: 'contenido sub 1',
            idst: 1
          },
          {
            titulo: 'titulo 2',
            contenido: 'contenido sub 2',
            idst: 2
          }*/
        ]
    },
    {
        idx: 1,
      titulo: "Factorización caso 1 al 5",
      contenido: [ {
        titulo: 'CASO I EL PRIMER CASO DE FACTORES SE DIVIDE EN DOS PARTES QUE SON: FACTOR COMÚN MONOMIO Y FACTOR COMÚN POLINOMIO FACTOR COMÚN MONOMIO',
        contenido: 'Es una expresión algebraica en la que se utilizan exponentes naturales de variables literales que constan de un solo término si hubiera + ó – seria binomio, un número llamado coeficiente. Las únicas operaciones que aparecen entre las letras son el producto y la potencia de exponentes naturales. Se denomina polinomio a la suma de varios monomios. Un monomio es una clase de polinomio con un único término. ',
        teorema: '',
        ejemplo: '1'
      },
      {
        titulo: 'CASO II FACTOR COMUN POR AGRUPACION',
        contenido: 'Se llama factor común por agrupación de términos, si los términos de un polinomio pueden reunirse en grupos de términos con un factor común diferente en cada grupo. Cuando pueden reunirse en grupos de igual número de términos se le saca en cada uno de ellos el factor común. Si queda la misma expresión en cada uno de los grupos entre paréntesis, se la saca este grupo como factor común, quedando así una multiplicación de polinomios. ratar desde el principio que nos queden iguales los términos de los paréntesis nos hará más sencillo el resolver estos problemas. ',
        teorema: '',
        ejemplo: '2'
      },
      {
        titulo: 'CASO III TRINOMIO CUADRADO PERFECTO',
        contenido: 'Es igual al cuadrado de un binomio. Se llama trinomio cuadrado perfecto al trinomio (polinomio de tres términos) tal que, dos de sus términos son cuadrados perfectos y el otro término es el doble producto de las bases de esos cuadrados. ',
        teorema: '',
        ejemplo: '3'
      },
      {
        titulo: 'CASO IV DIFERENCIA DE CUADRADOS',
        contenido: 'Se identifica por tener dos términos elevados al cuadrado y unidos por el signo menos. Se resuelve por medio de dos paréntesis, (parecido a los productos de la forma), uno positivo y otro negativo. En los paréntesis deben colocarse las raíces. ',
        teorema: '',
        ejemplo: '4'
      },
      {
        titulo: 'CASO V TRINOMIO CUADRADO PERFECTO POR ADICION Y SUSTRACCION',
        contenido: 'Algunos trinomios no cumplen las condiciones para ser trinomios cuadrados perfectos, el primer y tercer término tienen raíz cuadrada perfecta pero el de la mitad no es el doble producto de las dos raíces. Se debe saber cuanto debe ser el doble producto y la cantidad que falte para cuadrar el término de la mitad, esta cantidad se le suma y se le resta al mismo tiempo, de tal forma se armara un trinomio cuadrado y factorizado unido con el último término tendremos una diferencia de cuadrados. ',
        teorema: '',
        ejemplo: '5'
      },
    ],
      subtemas: [

      ]
    },
    {
        idx: 2,
        titulo: "Factorización caso 6 al 10",
        contenido: [
          {
            titulo: 'CASO VI TRINOMIO DE LA FORMA x2 + bx + c',
            contenido: 'Cumplen las condiciones siguientes: 1.El coeficiente del primer término es 1 , 2.El primer término es una letra cualquiera elevada al cuadrado. 3.El segundo término tiene la misma letra que el primero con exponente 1 y su coeficiente es una cantidad cualquiera, positiva o negativa. 4El tercer término es independiente de la letra que aparece en el primer y segundo término y es una cantidad cualquiera, positiva o negativa.',
            teorema: '',
            ejemplo: '1'
          },          {
            titulo: 'CASO VII TRINOMIO DE LA FORMA ax2 + bx + c',
            contenido: 'El primer término tiene un coeficiente mayor que 1 y tiene una letra cualquiera elevada al cuadrado. El segundo término tiene la misma letra que el primero pero con exponente 1 y su coeficiente es una cantidad cualquiera positiva o negativa. El tercer término es una cantidad cualquiera positiva o negativa sin ninguna letra en común con el 1 y 2 términos.',
            teorema: '',
            ejemplo: '2'
          },          {
            titulo: 'CASO VIII CUBO PERFECTO DE BINOMIOS',
            contenido: 'Debemos tener en cuenta que los productos notables nos dicen que: (a+b)3 = a2 +3a 2 b+3 a b 2 +b3 y (a-b)3 = a2-3a 2 b+3ab 2 - b3  La fórmula anterior nos dice que para una expresión algebraica ordenada con respecto a una parte literal sea el cubo de un binomio, tiene que cumplir lo siguiente: 1.Tener cuatro términos. 2.Que el primer término y el último sean cubos perfectos. 3.Que el segundo término sea más o menos el triplo de la primera raíz cúbica elevada al cuadrado que multiplica la raíz cúbica del último término. 4.Que el tercer término sea el triplo de la primera raíz cúbica por la raíz cubica del último término elevada al cuadrado. Si todos los términos de la expresión algebraica son positivos, la respuesta de la expresión dada será la suma de sus raíces cúbicas de su primer y último término, y si los términos son positivos y negativos la expresión será la diferencia de dichas raíces.',
            teorema: '',
            ejemplo: '3'
          },          {
            titulo: 'CASO IX SUMA O DIFERENCIA DE CUBOS PERFECTOS',
            contenido: '1. Descomponemos en dos factores. 2. En el primer factor se escribe la suma o la diferencia según sea el caso, de las raíces cúbicas de los dos términos. 3. En el segundo factor se escribe la raíz del primer termino elevada al cuadrado, empezando con el signo menos y de ahí en adelante sus signos alternados (si es una suma de cubos) o con signo más (si es una diferencia de cubos) el producto de la primera raíz por la segunda, más el cuadrado de la segunda raíz. La fórmula (1) nos dice: REGLA 1 la suma de dos cubos perfectos se descompone en dos factores: 1. La suma de sus raíces cúbicas 2. El cuadrado de la primera raíz, menos la multiplicación de las dos raíces, más el cuadrado de la segunda raíz. a3 +b3 =(a+b) (a2-ab+b2) La fórmula (2) nos dice: REGLA 2 La diferencia de dos cubos perfectos se descompone en dos factores: 1. La diferencia de sus raíces cúbicas 2. El cuadrado de la primera raíz, más el cuadrado de la segunda raíz.',
            teorema: '',
            ejemplo: '4'
          },          {
            titulo: 'CASO X SUMA O DIFERENCIA DE DOS POTENCIAS IGUALES',
            contenido: 'Se aplican los siguientes criterios: Criterios de divisibilidad de expresiones de la forma an + - bn Criterio 1: an – bn  es divisible por a - b siendo n par o impar  Criterio 2: an – bn  es divisible por a + b siendo n impar  Criterio 3: an – bn  es divisible por a + b siendo n es par  Criterio 4: an + bn  nunca es divisible por a - b .',
            teorema: '',
            ejemplo: '5'
          },
        ],
        subtemas: [
        ]
    },
    {
        idx: 3,
      titulo: "Leyes de los exponentes y radicales",
      contenido: [
        {
          titulo: 'Leyes de los exponentes',
          contenido: ' La finalidad de las leyes de los exponentes es resumir una expresión numérica que, si se expresa de manera completa y detallada sería muy extensa. Por esta razón es que en muchas expresiones matemáticas se encuentran expuestas como potencias.  23 es lo mismo que (2) ∙ (2) ∙ (2) = 8. Es decir, se debe multiplicar 2 tres veces. De esta manera, la expresión numérica es más simple y menos confusa para resolver.',
          teorema: '',
          ejemplo: ''
        },
        {
          titulo: 'Potencia con exponente 0',
          contenido: ' Cualquier número elevado a un exponente 0 es igual a 1. Cabe destacar que la base siempre debe ser diferente a 0, es decir a ≠ 0.',
          teorema: '',
          ejemplo: '1'
        },
        {
          titulo: 'Potencia con exponente 1',
          contenido: ' Cualquier número elevado a un exponente 1 es igual a sí mismo.',
          teorema: '',
          ejemplo: '2'
        },
        {
          titulo: 'Producto de potencias de igual base o multiplicación de potencias de igual base',
          contenido: ' ¿Qué pasa si tenemos dos bases (a) iguales con diferentes exponentes (n)? Es decir, an ∙ am. En este caso, las bases iguales se mantienen y se suman sus potencias, es decir: an ∙ am = an+m. Esto sucede porque el exponente es el indicador de cuántas veces se debe multiplicar el número base por sí mismo. Por tanto, el exponente final será la suma o resta de los exponentes que tienen una misma base.',
          teorema: '',
          ejemplo: '3'
        },
        {
          titulo: 'División de potencias de igual base o cociente de dos potencias con igual base',
          contenido: ' El cociente de dos potencias de igual base es igual a elevar la base según la diferencia del exponente del numerador menos el denominador. La base debe ser diferente a 0.',
          teorema: '',
          ejemplo: '4'
        },
        {
          titulo: 'Potencia de un producto o Ley distributiva de la potenciación con respecto de la multiplicación',
          contenido: 'Esta ley establece que la potencia de un producto debe ser elevada al mismo exponente (n) en cada uno de los factores.',
          teorema: '',
          ejemplo: '5'
        },
        {
          titulo: 'Potencia de otra potencia',
          contenido: 'Se refiere a la multiplicación de potencias que tienen las mismas bases, de la cual se obtiene una potencia de otra potencia.',
          teorema: '',
          ejemplo: '6'
        },
        {
          titulo: 'Ley del exponente negativo',
          contenido: ' Si se tiene una base con un exponente negativo (a-n) se debe tomar la unidad divida entre la base que será elevada con el signo del exponente en positivo, es decir 1/an . En este caso, la base (a) debe ser diferente a 0, a ≠ 0.',
          teorema: '',
          ejemplo: '7'
        },
        {
          titulo: 'Leyes de los radicales',
          contenido: 'La ley de los radicales se trata de una operación matemática que nos permite hallar la base a través de la potencia y el exponente. Los radicales son las raíces cuadras que se expresan de la siguiente manera √, y consiste en conseguir un número que multiplicado por sí mismo dé como resultado lo que está en la expresión numérica. Por ejemplo, la raíz cuadrada de 16 se expresa de la siguiente manera: √16 = 4; esto significa que 4.4 = 16. En este caso no es necesario indicar el exponente dos en la raíz. Sin embargo, en el resto de las raíces sí.',
          teorema: '',
          ejemplo: '8'
        },
        {
          titulo: '1. Ley de cancelación del radical',
          contenido: 'Una raíz (n) elevada a la potencia (n) se cancela.',
          teorema: '',
          ejemplo: '9'
        },
        {
          titulo: '2. Raíz de una multiplicación o producto',
          contenido: 'Una raíz de una multiplicación se puede separar como una multiplicación de raíces, sin importar el tipo de raíz.',
          teorema: '',
          ejemplo: '10'
        },
        {
          titulo: '3. Raíz de una división o cociente',
          contenido: 'La raíz de una fracción es igual a la división de la raíz del numerador y de la raíz del denominador.',
          teorema: '',
          ejemplo: '11'
        },
        {
          titulo: '4. Raíz de una raíz',
          contenido: 'Cuando dentro de una raíz hay una raíz se pueden multiplicar los índices de ambas raíces a fin de reducir la operación numérica a una sola raíz, y se mantiene el radicando.',
          teorema: '',
          ejemplo: '12'
        },
        {
          titulo: '5. Raíz de una potencia',
          contenido: 'Cuando se tiene dentro de una raíz un número elevado un exponente, se expresa como el número elevado a la división del exponente entre el índice del radical.',
          teorema: '',
          ejemplo: '13'
        },
      ],
      subtemas: [
      ]
    },
    {
        idx: 4,
        titulo: "Funciones y Graficas",
        contenido: [
          {
            titulo: 'La gráfica de una función',
            contenido: 'A continuación discutiremos algunos tipos importantes de funciones y observaremos sus gráficas. Pon atención a la forma que tienen las gráficas de estas funciones. Todos los ejemplos son de funciones algebráicas, discutiremos otros tipos de funciones, como las funciones trigonométricas, más adelante. Por lo pronto, observa las siguientes funciones y sus gráficas.',
            teorema: '',
            ejemplo: './xd'
          },
          {
            titulo: 'Función constante:',
            contenido: '',
            teorema: '',
            ejemplo: '1'
          },
          {
            titulo: 'Función identidad: ',
            contenido: '',
            teorema: '',
            ejemplo: '2'
          },
          {
            titulo: 'Función lineal: ',
            contenido: '',
            teorema: '',
            ejemplo: '3'
          },
          {
            titulo: 'Función afín: ',
            contenido: '',
            teorema: '',
            ejemplo: '4'
          },
          {
            titulo: 'Función cuadrática: ',
            contenido: '',
            teorema: '',
            ejemplo: '5'
          },
          {
            titulo: 'Función parte entera de x:',
            contenido: '',
            teorema: '',
            ejemplo: '6'
          },
          {
            titulo: 'Función mantisa: ',
            contenido: '',
            teorema: '',
            ejemplo: '7'
          },
          {
            titulo: 'Función signo:',
            contenido: '',
            teorema: '',
            ejemplo: '8'
          },
          {
            titulo: 'Función racional:',
            contenido: '',
            teorema: '',
            ejemplo: '9'
          },
          {
            titulo: 'Función exponencial: ',
            contenido: '',
            teorema: '',
            ejemplo: '10'
          },
          {
            titulo: 'Función logarítmica: ',
            contenido: '',
            teorema: '',
            ejemplo: '11'
          },
          {
            titulo: 'Función Seno:',
            contenido: '',
            teorema: '',
            ejemplo: '12'
          },
          {
            titulo: 'Función coseno:',
            contenido: '',
            teorema: '',
            ejemplo: '13'
          },
          {
            titulo: 'Función tangente:',
            contenido: '',
            teorema: '',
            ejemplo: '14'
          },
          {
            titulo: 'Función cotangente:',
            contenido: '',
            teorema: '',
            ejemplo: '15'
          },
          {
            titulo: 'Función secante:',
            contenido: '',
            teorema: '',
            ejemplo: '16'
          },
          {
            titulo: 'Función cosecante:',
            contenido: '',
            teorema: '',
            ejemplo: '17'
          }
        ],
        subtemas: [
        ]
    },
    {
        idx: 5,
      titulo: "Función inversa",
      contenido: [
        {
          titulo: 'Inversa',
          contenido: 'Se llama función inversa o reciproca de f(x)  a otra función  f^-1(x) que cumple que: Si f(a) = b, entonces f^-1(b) = a, Ejemplo a partir de la función f(x) = x + 4 ' ,
          teorema: '',
          ejemplo: '1'
        },
        {
          titulo: 'Cálculo de la función inversa',
          contenido: 'Para construir o calcular la función inversa de una función cualquiera, se deben seguir los siguientes pasos: Paso 1: Se escribe la función con xey . Paso 2: Se despeja la variable x en función de la variable y . Paso 3: Se intercambian las variables.' ,
          teorema: '',
          ejemplo: '2'
        }
      ],
      subtemas: [
      ]
    },
    {
        idx: 6,
        titulo: "Trigonometría",
        contenido: [
          {
            titulo: '',
            contenido: 'A menos que estés inmerso en el mundo de las matemáticas, es probable que definir un concepto como “trigonometría” resulte una tarea un tanto dificultosa…Sin embargo, ¡no desesperemos! Siempre podemos recurrir a la etimología de una palabra para abordar su significado. Incluso no habiendo estudiado latín o griego, podemos extraer la esencia de este concepto a partir del análisis de la propia palabra: Trigonometría, del griego trígonos (triángulo) y metrón (medida). Así pues, la trigonometría es una rama de la geometría (que a su vez es una rama de las matemáticas) encargada de estudiar la relación entre los lados y los ángulos de los triángulos; es decir, las medidas de los triángulos. Todos sabemos dibujar un triángulo. Es una de las primeras formas que aprendemos. ¿Quién no dibujaba los tejados de las casas con un triángulo cuando éramos niños? Pero una cosa es representarlo gráficamente, y otra muy distinta, definirlo… Un triángulo (o trígono) es simplemente una figura de tres lados; un polígono determinado por tres segmentos que se cortan dos a dos en tres puntos. Esos puntos son los vértices. Y los segmentos de las rectas que se cortan son los lados del triángulo. Dos lados contiguos forman un ángulo. Tal y como su nombre indica, un tri-ángulo tiene tres ángulos. Empecemos por lo básico. Para comprender lo que es la trigonometría, debemos saber primero qué es un ángulo: El ángulo es la porción del plano comprendida entre dos semirrectas que comparten un origen común llamado vértice. No es una definición muy sencilla a priori, lo sabemos, pero podría decirse que un ángulo es el “espacio” que se abre entre dos rectas que se cruzan sobre un plano. En un triángulo, sería el espacio que se abre entre dos lados unidos por un mismo vértice. Medir un ángulo, sería medir ese espacio. ' ,
            teorema: '',
            ejemplo: '1'
          },
          {
            titulo: 'Para medir ángulos existen tres unidades',
            contenido: 'El radián (utilizado sobre todo en matemáticas), El grado sexagesimal (representado: º). Es la medida más conocida. Está establecido que un plano mide 360º, lo que mide una circunferencia, El sistema decimal (utilizado en topografía y en construcción), Se hace uso de la trigonometría en campos como la medición de distancias entre estrellas o entre puntos geográficos, en construcción etc. No hay más que fijarse en las pirámides de Egipto para darse cuenta de que ya en el Antiguo Egipto y Babilonia los estudiosos de esta ciencia estaban al tanto de teoremas y funciones acerca de la medición de triángulos. Sin embargo; los fundamentos de la actual trigonometría se desarrollaron en la Antigua Grecia,  Un triángulo rectángulo es cualquier triángulo en el que dos de los lados formen un ángulo recto, es decir, un ángulo de 90 grados, En un triángulo rectángulo, el lado más largo se llama hipotenusa, y los otros dos lados se llaman catetos. La hipotenusa es el lado opuesto al ángulo recto del triángulo, Los catetos se llamarán cateto contrario o cateto adyacente dependiendo del ángulo que se mire, El teorema de Pitágoras establece que en todo triángulo rectángulo, el cuadrado de la hipotenusa es igual a la suma de los cuadrados de los catetos. Es decir, la hipotenusa es igual a la suma de los catetos. En un triángulo rectángulo con catetos a y b, e hipotenusa c, la fórmula será la siguiente: a^2 = b^2 + c^2  Tal y como decíamos, la trigonometría nos permite crear relaciones entre los lados y los ángulos de un triángulo. Existen determinadas funciones para medir estas relaciones en triángulos rectángulos. Estas funciones son las de seno, coseno y tangente. Para obtener el seno de un ángulo determinado se debe dividir la longitud del cateto opuesto y el de la hipotenusa (es decir cateto opuesto ÷ hipotenusa: o÷h), El cosenos e obtiene a partir de la relación entre la longitud del cateto adyacente y la hipotenusa (cateto adyacente ÷ hipotenusa: a÷h), Para obtener la tangente se divide la longitud de ambos catetos, (Cateto opuesto ÷ cateto adyacente o÷a), Aun así, si todo esto parece muy complicado de memorizar (que lo es…) te dejamos una regla mnemotécnica: SOH-CAH-TOA = Seno=Opuesto÷Hipotenusa, Coseno=Adyacente÷Hipotenusa, Tangente=Opuesto÷Adyacente. ' ,
            teorema: '',
            ejemplo: '2'
          }
        ],
        subtemas: [
        ]
    },
    {
        idx: 7,
      titulo: "Ley de seno y coseno",
      contenido: [
        {
          titulo: 'Ley de Seno',
          contenido: 'La ley de los senos es la relación entre los lados y ángulos de triángulos no rectángulos (oblicuos). Simplemente, establece que la relación de la longitud de un lado de un triángulo al seno del ángulo opuesto a ese lado es igual para todos los lados y ángulos en un triángulo dado. En ∆ABC es un triángulo oblicuo con lados a, b y c , entonces Para usar la ley de los senos necesita conocer ya sea dos ángulos y un lado del triángulo (AAL o ALA) o dos lados y un ángulo opuesto de uno de ellos (LLA). Dese cuenta que para el primero de los dos casos usamos las mismas partes que utilizó para probar la congruencia de triángulos en geometría pero en el segundo caso no podríamos probar los triángulos congruentes dadas esas partes. Esto es porque las partes faltantes podrían ser de diferentes tamaños. Ejemplo: Dado a = 22, b =12 y A = 40°. Encuentre los otros ángulos y el lado.  ',
          teorema: 'teo1',
          ejemplo: '1'
        },
        {
          titulo: 'Ley de Coseno',
          contenido: 'La ley de los cosenos es usada para encontrar las partes faltantes de un triángulo oblicuo (no rectángulo) cuando ya sea las medidas de dos lados y la medida del ángulo incluído son conocidas (LAL) o las longitudes de los tres lados (LLL) son conocidas. En cualquiera de estos casos, es imposible usar la ley de los senos porque no podemos establecer una proporción que pueda resolverse. La ley de los cosenos establece: a^2 = b^2 + c^2 - 2 ac cos  B or, a^2 = b^2 - c^2 - 2 bc cos A, Ejemplo: Dado a = 11, b = 5 y C = 20°. Encuentre el lado y ángulos faltantes.',
          teorema: 'teo2',
          ejemplo: '2'
        },
        {
          titulo: 'Teorema o ley de la tangente',
          contenido: 'Si A y B son ángulos de un triángulo y sus lados correspondientes son a y b, se cumple que: ',
          teorema: '',
          ejemplo: '3'
        }
      ],
      subtemas: [
      ]
    },
    {
        idx: 8,
        titulo: "Límites",
        contenido: [
          {
            titulo: 'Limites',
            contenido: 'En matemática, el concepto de límite es una noción topológica que formaliza la noción intuitiva de aproximación hacia un punto concreto de una sucesión o una función, a medida que los parámetros de esa sucesión o función se acercan a un determinado valor. En cálculo (especialmente en análisis real y matemático) este concepto se utiliza para definir los conceptos fundamentales de convergencia, continuidad, derivación, integración, entre otros.  En análisis real para funciones de una variable, se puede hacer una definición de límite similar a la de límite de una sucesión, en la cual, los valores que toma la función dentro de un intervalo se van aproximando a un punto fijado c , independientemente de que éste pertenezca al dominio de la función. Esto se puede generalizar aún más a funciones de varias variables o funciones en distintos espacios métricos. Informalmente, se dice que el límite de la función f(x) es L cuando x tiende a c, y se escribe: lim x->c f(x) = L',
            teorema: '',
            ejemplo: '1'
          },
          {
            titulo: 'Limites Finitos',
            contenido: 'Como dijimos anteriormente, que este límite exista significa que cuando x "se acerca" al punto a, f(x) "se acerca" al punto b. La cuestión que se plantea inmediatamente es ¿qué entendemos por estar cerca? ¿a partir de qué momento se considera que dos números están cerca? Para cuantificar este acercamiento utilizaremos la siguiente notación: ',
            teorema: '',
            ejemplo: ''
          },
          {
            titulo: 'Limites Infinito',
            contenido: 'La idea intutitiva de esta situación nos decía que cuando x se acerca al punto a, f(x) va creciendo indefinidamente, es decir, podemos hacer que f(x) sea tan grande como se quiera sin más que hacer que x se acerque suficientemente al punto a. De nuevo nos encontramos con conceptos algo ambiguos: "acercarse" y "hacerse grande". Al igual que en el caso anterior la cuestión principal es ¿a partir de qué valor consideramos que un número es grande? y ¿a qué distancia consideramos que dos números están cercanos?. Para responder a estas preguntas procederemos igual que en la situación anterior, es decir, partiremos de una situación concreta sobre la que se plantean una serie de cuestiones. Las respuestas a estas cuestiones nos permitirán definir con claridad los conceptos antes mencionados. ',
            teorema: '',
            ejemplo: ''
          }
        ],
        subtemas: [
        ]
    },
    {
        idx: 9,
      titulo: "Derivada",
      contenido: [
        {
          titulo: 'Derivada',
          contenido: 'En cálculo diferencial y análisis matemático, la derivada de una función es la razón de cambio instantánea con la que varía el valor de dicha función matemática, según se modifique el valor de su variable independiente. La derivada de una función es un concepto local, es decir, se calcula como el límite de la rapidez de cambio media de la función en cierto intervalo, cuando el intervalo considerado para la variable independiente se torna cada vez más pequeño. Por eso se habla del valor de la derivada de una función en un punto dado. Un ejemplo habitual aparece al estudiar el movimiento: si una función representa la posición de un objeto con respecto al tiempo, su derivada es la velocidad de dicho objeto para todos los momentos. Un avión que realice un vuelo transatlántico de 4500 km entre las 12:00 y las 18:00, viaja a una velocidad media de 750 km/h. Sin embargo, puede estar viajando a velocidades mayores o menores en distintos tramos de la ruta. En particular, si entre las 15:00 y las 15:30 recorre 400 km, su velocidad media en ese tramo es de 800 km/h. Para conocer su velocidad instantánea a las 15:20, por ejemplo, es necesario calcular la velocidad media en intervalos de tiempo cada vez menores alrededor de esta hora: entre las 15:15 y las 15:25, entre las 15:19 y las 15:21.  Entonces el valor de la derivada de una función en un punto puede interpretarse geométricamente, ya que se corresponde con la pendiente de la recta tangente a la gráfica de la función en dicho punto. La recta tangente es, a su vez, la gráfica de la mejor aproximación lineal de la función alrededor de dicho punto. La noción de derivada puede generalizarse para el caso de funciones de más de una variable con la derivada parcial y el diferencial. ',
          teorema: '',
          ejemplo: '1'
        }
      ],
      subtemas: [
      ]
    },
    {
        idx: 10,
        titulo: "Técnicas de derivación",
        contenido: [
          
        {
          titulo: 'La derivada de una constante',
          contenido: 'Según lo que hemos descubierto anteriormente la derivada de una constante es cero. Veamos un ejemplo.',
          teorema: '',
          ejemplo: '2'
        },
        {
          titulo: 'La derivada de una potencia entera positiva',
          contenido: 'Como ya sabemos, la derivada de xn es n xn-1, entonces:',
          teorema: '',
          ejemplo: '3'
        },
        {
          titulo: 'La derivada de una constante por una función.',
          contenido: 'Para derivar una constante por una función, es decir cf(x), su derivada es la constante por la derivada de la función, o cf(x), por ejemplo:>',
          teorema: '',
          ejemplo: '4'
        },
        {
          titulo: 'La derivada de una suma',
          contenido: "Tampoco podemos diferenciar (o derivar) una suma de funciones. La regla para la derivada de una suma es (f+g)'=f'+g', es decir, la derivada de una suma de funciones es la suma de las derivadas de cada uno de los términos por separado. Entonces:",
          teorema: '',
          ejemplo: '5'
        },
        {
          titulo: 'La derivada de un producto',
          contenido: "Aún no hemos dicho cual es la regla para derivar un producto de funciones, la regla para la derivada de un producto es (fg)'= fg'+f'g. En español esto se interpreta como la derivada de un producto de dos funciones es la primera, por la derivada de la segunda, más la segunda por la derivada de la primera.",
          teorema: '',
          ejemplo: '6'
        },
        {
          titulo: 'La derivada de un cociente',
          contenido: "  Traducción: la derivada de un cociente de dos funciones es (la segunda, por la derivada de la primera, menos la primera por la derivada de la segunda) entre la segunda al cuadrado.",
          teorema: '',
          ejemplo: '7'
        },
        {
          titulo: 'Las derivadas de las funciones trigonométricas',
          contenido: "Ahora daremos las fórmulas para las derivadas de las funciones trigonométricas.",
          teorema: '',
          ejemplo: '8'
        },
        {
          titulo: 'La derivada de una potencia entera de una función f.',
          contenido: "Sea y=[f (x)]n , entonces: y'=n[f(x)](n-1) f '(x) ",
          teorema: '',
          ejemplo: '10'
        }
        ],
        subtemas: [
        ]
    },
    {
        idx: 11,
      titulo: "Regla de la cadena",
      contenido: [
        {
          titulo: 'La regla de la cadena',
          contenido: "En términos intuitivos, si una variable y, depende de una segunda variable u, que a la vez depende de una tercera variable x; entonces, la razón de cambio de y con respecto a x puede ser calculada con el producto de la razón de cambio de y con respecto a u multiplicado por la razón de cambio de u con respecto a x.  Las reglas de derivación que hemos definido hasta ahora no permiten encontrar la derivada de una función compuesta como (3x + 5)4, a menos que desarrollemos el binomio y luego se apliquen las reglas ya conocidas. Observa el siguiente ejemplo.",
          teorema: '',
          ejemplo: '9'
        },
      ],
      subtemas: [
      ]
    },
    {
        idx: 12,
        titulo: "Derivadas Logaritmicas",
        contenido: [
          {
            titulo: 'Derivada del logaritmo natural',
            contenido: 'Por variados procedimientos, partiendo de distintas suposiciones, se puede demostrar que',
            teorema: '',
            ejemplo: '1'
          },
          {
            titulo: 'Derivada de logaritmo de función: ln(u(x))',
            contenido: ' La regla de la cadena se puede combinar con la derivada del logaritmo natural para obtener la derivada de logaritmo compuesto con una función, ln(u(x))',
            teorema: '',
            ejemplo: '2'
          },
          {
            titulo: 'Derivadas de logaritmos de productos, cocientes y potencias',
            contenido: 'Si tenemos un logaritmo de un producto, o de un cociente o de una potencia, conviene, antes de derivar, reescribirlo, usando las propiedades de los logaritmos, hasta que los argumentos de los logaritmos no sean productos, cocientes o potencias. En la primera animación diferenciamos de dos maneras, reescribiendo y sin reescribir. En la segunda animación mostramos dos ejemplos en que conviene sin duda reescribir el logaritmo antes de derivar. ',
            teorema: '',
            ejemplo: '3'
          },
          {
            titulo: 'Derivada de la función logarítmica general',
            contenido: 'Mediante la fórmula de cambio de base podemos obtener la derivada del logaritmo de x con base b. Primero pasamos el logaritmo a base e , Reescribimos antes de derivar como el producto de un factor constante por el logaritmo natural de x , Ya podemos diferenciar, tomando en cuenta que lnb es una constante y por tanto, la fracción también lo es ,cambiamos el orden, para recordarnos que es la derivada del logaritmo natural por un factor numérico.',
            teorema: '',
            ejemplo: '4'
          },
        ],
        subtemas: [
        ]
    },
    {
        idx: 13,
      titulo: "Razones de Cambio",
      contenido: [
        {
          titulo: 'Razones de Cambio',
          contenido: 'El concepto de razón de cambio se refiere a la medida en la cual una variable se modifica con relación a otra. Se trata de la magnitud que compara dos variables a partir de sus unidades de cambio. En caso de que las variables no estén relacionadas, tendrán una razón de cambio igual a cero. La razón de cambio más frecuente es la velocidad, que se calcula dividiendo un trayecto recorrido por una unidad de tiempo. Esto quiere decir que la velocidad se entiende a partir del vínculo que se establece entre la distancia y el tiempo. De acuerdo a cómo se modifica la distancia recorrida en el tiempo por el movimiento de un cuerpo, podemos conocer cuál es su velocidad. Supongamos que un automóvil recorre 100 kilómetros en dos horas. La razón de cambio existente entre ambas variables es 50 kilómetros por hora. Ese valor representa su velocidad, ya que v = d / t (velocidad = distancia / tiempo). A partir del conocimiento de una razón de cambio, es posible desarrollar diferentes cálculos y previsiones. Si conocemos el nivel de contaminación que está llegando a un arroyo a partir del vertido de sustancias químicas por parte de una industria, es posible utilizar la razón de cambio para señalar qué tan rápido se incrementa el nivel de contaminación. Es posible distinguir entre dos tipos de razón de cambio: la promedio y la instantánea, las cuales se explican a continuación. Es importante resaltar que haciendo uso de estos conceptos, se abren las puertas a la solución de ciertos problemas para los cuales los métodos algebraicos no son efectivos.',
          teorema: '',
          ejemplo: '1'
        },
        {
          titulo: 'Razón de cambio promedio',
          contenido: 'Nuestro día a día nos enfrenta a diversas razones de cambio de situaciones sociales, económicas y naturales, entre otras, en las cuales deseamos saber cuál es el valor más grande o el más pequeño (el máximo y el mínimo, respectivamente), su crecimiento o su disminución en un período de tiempo determinado. Se trata de problemas en los cuales estudiamos fenómenos relacionados con la variación de una magnitud que depende de otra, por lo cual es necesaria una descripción y una cuantificación de dichos cambios por medio de gráficas, tablas y modelos matemáticos. Así como en el ejemplo del coche que recorre 100 kilómetros en dos horas, los problemas que nos llevan a calcular la razón de cambio promedio arrojan resultados en los cuales se determina una variación que no necesariamente existe en la realidad a cada momento; en otras palabras, no sabemos si el coche ha mantenido esta velocidad a lo largo de las dos horas, sino que estimamos el promedio de unidades de distancia al cual debió avanzar para completar dicho recorrido.',
          teorema: '',
          ejemplo: '2'
        },
        {
          titulo: 'Razón de cambio instantánea',
          contenido: 'La razón de cambio instantánea también se denomina segunda derivada y hace referencia a la velocidad con la cual cambia la pendiente de una curva en un momento determinado. No olvidemos que la razón de cambio muestra la proporción en la que cambia una variable con respecto a otra o, desde un punto de vista gráfico, la pendiente de una curva. Si retomamos el ejemplo del coche, la razón de cambio instantánea podría resultar útil para conocer el trayecto recorrido en un punto específico de las dos horas, que es el plazo de tiempo total analizado en el problema. A diferencia de la razón promedio, la instantánea tiene una visión muy puntual, ya que busca conocer o corregir valores antes de que finalice el periodo.',
          teorema: '',
          ejemplo: '3'
        }
      ],
      subtemas: [
      ]
    },
    {
        idx: 14,
        titulo: "Regla de L´Hopital",
        contenido: [
          {
            titulo: '',
            contenido: "Esta regla recibe su nombre en honor al matemático francés del siglo XVII Guillaume François Antoine, marqués de l'Hôpital (1661 - 1704), quien dio a conocer la regla en su obra Analyse des infiniment petits pour l'intelligence des lignes courbes (1696), el primer texto que se ha escrito sobre cálculo diferencial, aunque actualmente se sabe que la regla se debe a Johann Bernoulli, que fue quien la desarrolló y demostró. En matemática, más específicamente en el cálculo diferencial, la regla de l'Hôpital o regla de l'Hôpital-Bernoulli es una regla que usa derivadas para ayudar a evaluar límites de funciones que estén en forma indeterminada.",
            teorema: 'teo1',
            ejemplo: '2'
          }
        ],
        subtemas: [
        ]
    },
    {
        idx: 15,
      titulo: "Problemas de Optimización",
      contenido: [
        {
          titulo: 'Problemas de Optimizaion',
          contenido: 'En el caso más simple, un problema de optimización consiste en maximizar o minimizar una función real eligiendo sistemáticamente valores de entrada (tomados de un conjunto permitido) y computando el valor de la función. La generalización de la teoría de la optimización y técnicas para otras formulaciones comprende un área grande de las matemáticas aplicadas. De forma general, la optimización incluye el descubrimiento de los "mejores valores" de alguna función objetivo dado un dominio definido, incluyendo una variedad de diferentes tipos de funciones objetivo y diferentes tipos de dominios.  Optimización hace referencia a la acción y efecto de optimizar. En términos generales, se refiere a la capacidad de hacer o resolver alguna cosa de la manera más eficiente posible y, en el mejor de los casos, utilizando la menor cantidad de recursos. En las últimas décadas, el término optimización se ha vinculado al mundo de la informática. Sin embargo, es un concepto que también se utiliza en las matemáticas, en la gestión de procesos y la economía. ',
          teorema: '',
          ejemplo: '1'
        }
      ],
      subtemas: [
      ]
    },
    {
        idx: 16,
        titulo: "Antiderivada e intragral indefinida",
        contenido: [
          {
            titulo: 'Antiderivada',
            contenido: 'La antiderivada es la función que resulta del proceso inverso de la derivación, es decir, consiste en encontrar una función que, al ser derivada produce la función dada.  La antiderivada también se conoce como la primitiva o la integral indefinida se expresa de la siguiente manera: en donde: f(x) es el integrando; dx, la variable de integración o diferencial de x y C es la constante de integración. ',
            teorema: 'teo',
            ejemplo: '1'
          },
          {
            titulo: 'Integral Indefinida',
            contenido: 'En cálculo infinitesimal, la función primitiva o antiderivada de una función f es una función F cuya derivada es f, es decir, F ′ = f. Una condición suficiente para que una función f admita primitivas sobre un intervalo es que sea continua en dicho intervalo. Si una función f admite una primitiva sobre un intervalo, admite una infinidad, que difieren entre sí en una constante: si F1 y F2 son dos primitivas de f, entonces existe un número real C, tal que F1 = F2 + C. A C se le conoce como constante de integración. Como consecuencia, si F es una primitiva de una función f, el conjunto de sus primitivas es F + C. A dicho conjunto se le llama integral indefinida de f y se representa como:  ',
            teorema: 'rep',
            ejemplo: '2'
          }
        ],
        subtemas: [ ]
    },
    {
        idx: 17,
      titulo: "Técnicas de Integración",
      contenido: [
        {
          titulo: 'Tecnicas de Integracion',
          contenido: 'Nos permite encontrar las integrales de una clase muy amplia de funciones. Todas las técnicas tienen como objetivo reducir la integral buscada a una integral ya conocida o inmediata. (concepto)',
          teorema: '',
          ejemplo: ''
        },
        {
          titulo: 'A. Integración por cambio de variable.',
          contenido: "Nos proporciona un proceso que permite reconocer cuándo un integrando es el resultado de una derivada en la que se ha usado la regla de la cadena. Para que la fórmula de cambio de variable tenga posibilidades de éxito, debemos identificar en el integrando a una función u y a u' (su derivada). Sea f(x) la función que deseamos integrar, entonces hacemos el siguiente cambio de variable: x = g(t), d(x) = g'(t)dt, con lo que:  ",
          teorema: 'teo1',
          ejemplo: '1'
        },
        {
          titulo: 'B. Integración por partes',
          contenido: 'Permite resolver integrales de funciones que pueden expresarse como un producto de una función por la derivada de otra.Sean u y v dos funciones continuas, derivables y sus derivadas du y dv son integrables, entonces:    ',
          teorema: 'teo2',
          ejemplo: '2'
        },
        {
          titulo: 'C. Integración de funciones racionales',
          contenido: 'Vamos a integrar funciones racionales (cociente de polinomios), que siguen la forma:',
          teorema: 'teo3',
          ejemplo: '3'
        },
        {
          titulo: 'D. Técnicas de Integración trigonométrica - Funciones racionales de funciones trigonométricas.',
          contenido: 'Si el integrando es una función racional de senos y cosenos de la forma R(senx, cosx), entonces la integral se reduce a la integral de una función racional de "t" mediante un cambio de variable.',
          teorema: '',
          ejemplo: '4'
        },
        {
          titulo: 'D. Técnicas de Integración trigonométrica -  Integrales que contienen funciones trigonométricas -1) Potencias de senos y cosenos. ',
          contenido: 'Vamos a integrar funciones racionales (cociente de polinomios), que siguen la forma:',
          teorema: 'teo5',
          ejemplo: '5'
        },
        {
          titulo: 'D. Técnicas de Integración trigonométrica -  Integrales que contienen funciones trigonométricas -2) Productos de potencias de senos y cosenos.  . ',
          contenido: 'Vamos a integrar funciones racionales (cociente de polinomios), que siguen la forma:',
          teorema: 'teo6',
          ejemplo: ''
        },
        {
          titulo: 'D. Técnicas de Integración trigonométrica -  Integrales que contienen funciones trigonométricas -3) Productos de potencias de tangentes y secantes.',
          contenido: 'Vamos a integrar funciones racionales (cociente de polinomios), que siguen la forma:',
          teorema: 'teo7',
          ejemplo: ''
        },
        {
          titulo: 'E. Sustitución trigonométrica. -1) Si en el integrando aparece un radical de la forma:',
          contenido: 'Este método nos permitirá integrar cierto tipo de funciones algebraicas cuyas integrales son funciones trigonométricas. ',
          teorema: 'teo8',
          ejemplo: '8'
        },
        {
          titulo: 'E. Sustitución trigonométrica. -2) Si en el integrando aparece un radical de la forma:',
          contenido: 'Este método nos permitirá integrar cierto tipo de funciones algebraicas cuyas integrales son funciones trigonométricas. ',
          teorema: 'teo9',
          ejemplo: '9'
        },
        {
          titulo: 'E. Sustitución trigonométrica. -3) Si en el integrando aparece un radical de la forma:',
          contenido: 'Este método nos permitirá integrar cierto tipo de funciones algebraicas cuyas integrales son funciones trigonométricas. ',
          teorema: 'teo10',
          ejemplo: ''
        },
        {
          titulo: 'F. Integración de funciones Irracionales - A',
          contenido: 'Este método nos permitirá integrar cierto tipo de funciones algebraicas cuyas integrales son funciones trigonométricas. ',
          teorema: 'teo11',
          ejemplo: '11'
        },
        {
          titulo: 'F. Integración de funciones Irracionales - B',
          contenido: 'Este método nos permitirá integrar cierto tipo de funciones algebraicas cuyas integrales son funciones trigonométricas. ',
          teorema: 'teo12',
          ejemplo: '12'
        },
        {
          titulo: 'F.  Integración de funciones Irracionales - C',
          contenido: 'Este método nos permitirá integrar cierto tipo de funciones algebraicas cuyas integrales son funciones trigonométricas. ',
          teorema: 'teo13',
          ejemplo: '13'
        },
      ],
      subtemas: [ ]
    },
    {
        idx: 18,
        titulo: "Teorema fundamental del cálculo",
        contenido: [
          {
            titulo: 'Teorema Fundamental del Calculo',
            contenido: 'Proporciona un método abreviado para calcular integrales definidas, sin necesidad de tener que calcular los límites de las sumas de Riemann. El Teorema Fundamental del Cálculo Integral nos muestra que F(x) es precisamente el área limitada por la gráfica de una función continua f(x). El teorema fundamental del cálculo consiste (intuitivamente) en la afirmación de que la derivación e integración de una función son operaciones inversas.Esto significa que toda función acotada e integrable (siendo continua o discontinua en un número finito de puntos) verifica que la derivada de su integral es igual a ella misma. Este teorema es central en la rama de las matemáticas denominada análisis matemático o cálculo infinitesimal. El teorema fue fundamental porque hasta entonces el cálculo aproximado de áreas -integrales- en el que se venía trabajando desde Arquímedes, era una rama de las matemáticas que se seguía por separado del cálculo diferencial que se venía desarrollando por Isaac Newton, Isaac Barrow y Gottfried Leibniz en el siglo XVIII, y dio lugar a conceptos como el de las derivadas. Las integrales eran investigadas como formas de estudiar áreas y volúmenes, hasta que en ese punto de la historia ambas ramas convergieron, al demostrarse que el estudio del "área bajo una función" estaba íntimamente vinculado al cálculo diferencial, resultando la integración la operación inversa a la derivación. Una consecuencia directa de este teorema es la regla de Barrow, denominada en ocasiones segundo teorema fundamental del cálculo, y que permite calcular la integral de una función utilizando la integral indefinida de la función al ser integrada. ',
            teorema: 'teo1',
            ejemplo: '1'
          }
        ],
        subtemas: [
        ]
    },
    {
        idx: 19,
      titulo: "Área entre curvas",
      contenido: [
        {
          titulo: '',
          contenido: 'Si consideramos dos funciones continuas en un intervalo [a, b] en f y g por encima del eje con los x con f por encima de g por encima del eje de los x y con f por encima de g en [a, b] es posible encontrar el area entre las curvas haciendo uso del concepto de integral. Se puede interpretar el area entre las curvas como la recta de las funciones f y g como se muestran en la figura.',
          teorema: 'teo1',
          ejemplo: '1'
        },
        {
          titulo: '',
          contenido: '',
          teorema: '',
          ejemplo: '2'
        }
      ],
      subtemas: [
        {
          titulo: '',
          contenido: '',
          idst: 1
        }
      ]
    },
    {
        idx: 20,
      titulo: "Volumen",
      contenido: [
        {
          titulo: 'Volumen',
          contenido: 'El volumen es una magnitud escalar definida como la extensión en tres dimensiones de una región del espacio.  Es una magnitud derivada de la longitud, ya que en un ortoedro se halla multiplicando tres longitudes: el largo, el ancho y la altura. Matemáticamente el volumen es definible no solo en cualquier espacio euclídeo, sino también en otro tipo de espacios métricos que incluyen por ejemplo a las variedades de Riemann. Desde un punto de vista físico, los cuerpos materiales ocupan un volumen por el hecho de ser extensos, fenómeno que se debe al principio de exclusión de Pauli. La noción de volumen es más complicada que la de superficie y en su uso formal puede dar lugar a la llamada paradoja de Banach-Tarski. ',
          teorema: '',
          ejemplo: '1'
        }
      ],
      subtemas: [ ]
    },
    {
        idx: 21,
        titulo: "Geometría del espacio",
        contenido: [
          {
            titulo: 'Geometría del espacio',
            contenido: 'La geometría del espacio (también llamada geometría espacial) es la rama de la geometría que se encarga del estudio de las figuras geométricas voluminosas que ocupan un lugar en el espacio; estudia las propiedades y medidas de las figuras geométricas en el espacio tridimensional o espacio euclídeo. Entre estas figuras, también llamadas sólidos, se encuentran el cono, el cubo, el cilindro, la pirámide, la esfera, el prisma, los poliedros regulares (los sólidos platónicos, convexos, y los sólidos de Kepler-Poinsot, no convexos) y otros poliedros. La geometría del espacio amplía y refuerza las proposiciones de la geometría plana, y es la base fundamental de la trigonometría esférica, la geometría analítica del espacio, la geometría descriptiva y otras ramas de las matemáticas. Se usa ampliamente en matemáticas, en ingeniería y en ciencias naturales. ',
            teorema: '',
            ejemplo: ''
          },
          {
            titulo: 'Sistema de coordenadas en R3',
            contenido: 'Un sistema de coordenadas tridimensional se construye trazando un eje Z, perpendicular en el origen de coordenadas a los ejes X e Y. Cada punto viene determinado por tres coordenadas P(x, y, z).Los ejes de coordenadas determinan tres planos coordenados: XY, XZ e YZ. Estos planos coordenados dividen al espacio en ocho regiones llamadas octantes, en el primer octante las tres coordenadas son positivas.  ',
            teorema: '',
            ejemplo: '1'
          },
          {
            titulo: 'Regla de la Mano Derecha',
            contenido: 'Si pones el dedo indice de tu mano derecha apuntando en el mismo sentido que el vector a-> y el dedo, mayor en el mismo sentido que b->, entonces el sentido del producto vectorial a-> x b-> de los vectores a y b lo da el pulgar de la misma mano derecha cuando se estira de manera que este perpendicular a los otros dos dedos.  . ',
            teorema: '',
            ejemplo: '2'
          },
          {
            titulo: 'Vector Unitario',
            contenido: 'Los vectores son, en el terreno de la física, magnitudes definidas por su punto de aplicación, su sentido, su dirección y su valor. Según el contexto en el que aparecen y sus características, se clasifican de distinto modo. La idea de vector unitario refiere al vector cuyo módulo es igual a 1. Cabe recordar que el módulo es la cifra coincidente con la longitud cuando el vector se representa en un gráfico. El módulo, de este modo, es una norma de la matemática que se aplica al vector que aparece en un espacio euclídeo. ',
            teorema: '',
            ejemplo: '3'
          },
          {
            titulo: 'Producto Punto',
            contenido: 'En matemáticas, el producto escalar también conocido como producto interno o producto punto, es una operación algebraica que toma dos secuencias de números de igual longitud (usualmente en la forma de vectores) y retorna un único número. Algebraicamente, el producto punto es la suma de los productos de las correspondientes entradas en dos secuencias de número. Geométricamente, es el producto de dos magnitudes euclidianas de los dos vectores y el coseno del ángulo entre ellos. El nombre del producto punto se deriva del símbolo que se utiliza para denotar esta operación (« · »). El nombre alternativo de producto escalar enfatiza el hecho del que el resultado es un escalar en lugar de un vector (en el caso de espacios de tres dimensiones)  . ',
            teorema: '',
            ejemplo: '4'
          }
        ],
        subtemas: [ ]
    },
    {
        idx: 22,
      titulo: "Derivadas parciales",
      contenido: [
        {
          titulo: 'Derivadas parciales',
          contenido: 'En cálculo diferencial, una derivada parcial de una función de diversas variables, es la derivada respecto a cada una de esas variables manteniendo las otras como constantes. Las derivadas parciales son útiles en cálculo vectorial, geometría diferencial, funciones analíticas, física, matemática, etc. La derivada parcial de una función f respecto a la variable x se representa con cualquiera de las siguientes notaciones equivalentes:  ',
          teorema: 'teo1',
          ejemplo: '1'
        }
      ],
      subtemas: [ ]
    },
    {
        idx: 23,
        titulo: "Regla de la cadena y vector gradiente",
        contenido: [
          {
            titulo: 'Regla de la Cadena',
            contenido: 'En cálculo, la regla de la cadena es una fórmula para la derivada de la composición de dos funciones. Tiene aplicaciones en el cálculo algebraico de derivadas cuando existe composición de funciones. Estudiaremos dos casos: cuando solo hay una variable independiente y cuando hay dos.',
            teorema: '',
            ejemplo: '1'
          },
          {
            titulo: 'Regla de la Cadena - Una variable independiente',
            contenido: '',
            teorema: 'teo11',
            ejemplo: ''
          },
          {
            titulo: 'Regla de la Cadena - Dos variables independientes',
            contenido: '',
            teorema: 'teo12',
            ejemplo: ''
          },
          {
            titulo: 'Vector Gradiente',
            contenido: 'Llamamos vector gradiente de una función f en el punto a, a un vector columna con n componentes, es decir, una matriz de orden nx1, donde n depende del número de variables en las que está definida f. Se denota como ∇f(a)',
            teorema: 'teo2',
            ejemplo: '2'
          }
        ],
        subtemas: [ ]
    },
    {
        idx: 24,
      titulo: "Integrales Dobles",
      contenido: [
        {
          titulo: '',
          contenido: 'Integrales Dobles Las integrales dobles son una manera de integrar sobre una región bidimensional. Entre otras cosas, nos permiten calcular el volumen bajo una superficie. Dada una función de dos variables, f(x, y), puedes encontrar el volumen entre la gráfica y una región rectangular del plano xy al tomar la integral de una integral esta es la función de y a esta integral se le conoce como integral doble.',
          teorema: 'teo1',
          ejemplo: '1'
        }
      ],
      subtemas: []
    },
    {
        idx: 25,
        titulo: "Integrales Triples",
        contenido: [
          {
            titulo: 'Integrales Triples',
            contenido: 'Representamos a una integral triple cuando buscamos integrar en base a una función 3 variables, en donde esta función pertenece a los reales en tres dimensiones. Así como la integral doble, la integral tripe tiene características muy parecidas a esta.  Veamos el caso de una triple integral sobre una caja rectangular y la definición formal de esta integral:',
            teorema: 'teo1',
            ejemplo: '1'
          }
        ],
        subtemas: []
    }
  ];

  constructor() {
    console.log("Servicio listo para usar!!!");
  }


  getTemas():Tema[]{
    return this.tema;
  }

  getHeroe( idx: number ){
    return this.tema[idx];
  }


}


export interface SubTema{
  titulo: string;
  contenido: string;
  idst: number;
};


export interface TemasGenerales{
  titulo: string;
  contenido: string;
  teorema: string;
  ejemplo: string;
};


export interface Tema{
  titulo: string;
  contenido: TemasGenerales[];
  subtemas: SubTema[];
  idx?: number;
};
