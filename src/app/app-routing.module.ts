import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContenidoComponent } from './components/shared/contenido/contenido.component';
import { InicioComponent } from './components/shared/inicio/inicio.component';
import { TemasComponent } from './components/shared/temas/temas.component';

const routes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'contenido/:id', component: ContenidoComponent },
  { path: 'temas', component: TemasComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
