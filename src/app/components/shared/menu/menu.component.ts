import { Component, OnInit } from '@angular/core';
import { DataService, Tema } from 'src/app/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  temas:Tema[] = [];


  constructor(private _temasService:DataService,
    private router:Router) { }

  ngOnInit() {
    this.temas = this._temasService.getTemas();
  }

}
