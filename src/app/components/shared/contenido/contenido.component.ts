import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-contenido',
  templateUrl: './contenido.component.html',
  styleUrls: ['./contenido.component.css']
})
export class ContenidoComponent {

  tema:any = {};

  constructor(private activatedRoute: ActivatedRoute,
    private _temaService: DataService) {
        this.activatedRoute.params.subscribe( params =>{
          this.tema = this._temaService.getHeroe( params['id'] );
          // console.log(this.tema);
      });
    }

}
